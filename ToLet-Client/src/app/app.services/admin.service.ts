import { Injectable } from '@angular/core';
import { ApiBase } from './service-base';
import { AuthService } from './auth.service';
import { ProfileViewModel } from '../app.view-models/profileViewModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AdminSetting } from '../app.view-models/adminSetting';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  adminApi: string = this.apiBase.baseUri + 'Admin/';

  constructor(private apiBase: ApiBase, private http: HttpClient, private auth: AuthService) { }

  getAllUsers() {
    return this.http.get<ProfileViewModel[]>(this.adminApi + 'GetUsers');
  }

  getAllSettings() {
    return this.http.get<AdminSetting[]>(this.adminApi + 'GetSettings');
  }

  setSettings(setting: AdminSetting) {
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + this.auth.getToken()
      })
    }
    return this.http.post<AdminSetting[]>(this.adminApi + 'SetSettings', setting, options);
  }

  approveComment(id: number){
    return this.http.get(this.adminApi + 'ApproveComment/' + id);
  }

  approveAd(id: number){
    return this.http.get(this.adminApi + 'ApproveAd/' + id);
  }
}
