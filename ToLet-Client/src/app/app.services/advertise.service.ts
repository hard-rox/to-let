import { Injectable } from '@angular/core';
import { ApiBase } from './service-base';
import { AdvertiseCategoryViewModel } from '../app.view-models/advertiseCategoryViewModel';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AdvertiseTypeViewModel } from '../app.view-models/advertiseTypeViewModel';
import { Observable } from 'rxjs';
import { AdvertiseViewModel } from '../app.view-models/advertiseViewModel';
import { AuthService } from './auth.service';
import { CommentVM } from '../app.view-models/commentVM';
import { AdminService } from './admin.service';

@Injectable({
  providedIn: 'root'
})
export class AdvertiseService {

  advertiseApi: string = this.apiBase.baseUri + 'Advertise/';

  constructor(private apiBase: ApiBase,
    private http: HttpClient,
    private auth: AuthService,
    private adminService: AdminService) { }

  getCategories() {
    return this.http.get<AdvertiseCategoryViewModel[]>(this.apiBase.baseUri + 'AdvertiseCategory');
  }

  getTypes(): Observable<AdvertiseTypeViewModel[]> {
    return this.http.get<AdvertiseTypeViewModel[]>(this.apiBase.baseUri + 'AdvertiseType');
  }

  saveAdvertise(model: AdvertiseViewModel): Observable<any> {
    console.log(model);
    let body = JSON.stringify(model);
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + this.auth.getToken()
      })
    }

    // this.adminService.getAllSettings().subscribe(
    //   data => {
    //     if(data[0].SettingValue) model.is
    //   }
    // );

    //console.log(body + JSON.stringify(options));
    return this.http.post(this.apiBase.baseUri + 'Advertise/CreateAdvertise', model, options);
  }

  getMyAdvertises() {
    let options = { headers: new HttpHeaders({ 'Authorization': 'bearer ' + this.auth.getToken() }) };
    return this.http.get<AdvertiseViewModel[]>(this.apiBase.baseUri + 'Advertise', options);
  }

  getAllAdvertises(id: number) {
    return this.http.get<AdvertiseViewModel[]>(this.apiBase.baseUri + 'Advertise/GetAllAdvertises/' + id);
  }

  getSearchedAdvertises(searchText: string) {
    return this.http.get<AdvertiseViewModel[]>(this.apiBase.baseUri + 'Advertise/SearchAd?searchText=' + searchText);
  }

  getAdvertise(id: number) {
    return this.http.get<AdvertiseViewModel>(this.advertiseApi + 'Get/' + id);
  }

  postComment(model: CommentVM): Observable<any> {
    let body = JSON.stringify(model);
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + this.auth.getToken()
      })
    }
    //console.log(body + JSON.stringify(options));
    return this.http.post(this.apiBase.baseUri + 'Advertise/PostComment', body, options);
  }

  deleteAdvertise(model: AdvertiseViewModel): Observable<any>{
    return this.http.delete<AdvertiseViewModel>(this.advertiseApi + 'Delete/' + model.Id);
  }
}
