﻿using System.Collections.Generic;

namespace ToLet.Models
{
	public class ProfileViewModel
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string ShortBio { get; set; }
		public string Address { get; set; }
		public string Email { get; set; }
		public string PhoneNo { get; set; }
		public string ProfileImage { get; set; }

		public virtual ICollection<Advertise> Advertises { get; set; }

		public ProfileViewModel(ApplicationUser user)
		{
			this.Id = user.Id;
			this.Name = user.Name;
			this.ShortBio = user.ShortBio;
			this.Address = user.Address;
			this.Email = user.Email;
			this.PhoneNo = user.PhoneNumber;
			this.Advertises = user.Advertises;
		}

		public ProfileViewModel() { }
	}
}