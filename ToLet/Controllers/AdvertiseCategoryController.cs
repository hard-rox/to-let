﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ToLet.BusinessLogics;
using ToLet.Models;

namespace ToLet.Controllers
{
    public class AdvertiseCategoryController : ApiController
    {
        private readonly AdvertiseCategoryLogics _advertiseCategoryLogics = new AdvertiseCategoryLogics();
		// GET: api/AdvertiseCategory
        public IEnumerable<AdvertiseCategory> Get()
        {
			try
			{
				return _advertiseCategoryLogics.GetAllAdvertiseCategory();
			}
			catch (Exception ex)
			{
				return new List<AdvertiseCategory>();
			}
        }
    }
}
