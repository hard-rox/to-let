import { Component, OnInit } from '@angular/core';
import { AdvertiseCategoryViewModel } from '../../app.view-models/advertiseCategoryViewModel';
import { AdvertiseTypeViewModel } from '../../app.view-models/advertiseTypeViewModel';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AdvertiseService } from '../../app.services/advertise.service';
import { AuthService } from 'src/app/app.services/auth.service';
import { AdvertiseViewModel } from 'src/app/app.view-models/advertiseViewModel';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    categories: Array<AdvertiseCategoryViewModel>;
    types: Array<AdvertiseTypeViewModel>;
    allAds: Array<AdvertiseViewModel>;

    constructor(private titleService: Title,
        private authService: AuthService,
        private adService: AdvertiseService) {
    }

    isLoggedIn(): boolean {
        return this.authService.checkLogin();
    }

    search(searchForm: any) {
        console.log(JSON.stringify(searchForm));
        //return false;

        //this.router.navigate(['/all-ad']);
        //this.adService.getSearchResults(new SearchCriteria())
        //    .subscribe(data => console.log(data));
    }

    ngOnInit() {
        this.titleService.setTitle('Home - ToLet');

        this.adService.getCategories().subscribe(data => {
            this.categories = data;
            console.log(data);
        },
            error => {
                console.error(error)
            });

        this.adService.getAllAdvertises(0).subscribe(data => {
            data = data.filter(ad => ad.IsApproved);
            data.sort((a: AdvertiseViewModel, b:AdvertiseViewModel) => {
                return new Date(b.PostingDate).getTime() - new Date(a.PostingDate).getTime();
            });
            this.allAds = data.slice(0, 10); 
            console.log(data);
        },
        err => {
            console.log(err);
        });
    }
}
