﻿import { AdvertiseTypeViewModel } from "./advertiseTypeViewModel";
import { CommentVM } from "./commentVM";
import { Picture } from "./Picture";

export class AdvertiseViewModel {
    Id: number;
    Title: string;
    Address: string;
    Rent: number;
    Size: number;
    NoOfBed: number;
    NoOfBath: number;
    Description: string;
    IsApproved: boolean;
    HasDrawingRoom: boolean;
    HasDiningRoom: boolean;
    HasCarParking: boolean;
    HasLift: boolean;
    HasGas: boolean;
    HasGenerator: boolean;
    IsElectrickBillIncluded: boolean;
    PostingDate: Date;
    AdvertiseCategoryId: number;
    PropertyOwnerId: string;
    Comments: CommentVM[];
    Pictures: Picture[]
}