﻿using ToLet.Models;

namespace ToLet.DataAccess
{
	public class CommentAccess:GenericDataAccess<Comment>
	{
		public CommentAccess(): base(ApplicationDbContext.Create()) { }
	}
}
