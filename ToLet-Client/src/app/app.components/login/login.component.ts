import { Component, OnInit } from '@angular/core';
import { LoginViewModel } from '../../app.view-models/loginViewModel';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../../app.services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  constructor(private titleService: Title,
    private accountService: AuthService,
    private router: Router) { }

  isLoading: boolean = false;
  serverError: string;

  onSubmit(loginForm: any) {
    this.isLoading = true;

    // setTimeout(() => {
    //   this.isLoading = false;
    // }, 100);

    var model = new LoginViewModel(loginForm.email, loginForm.password);
    this.accountService.login(model)
      .subscribe(data => {
        if(this.accountService.getUsername() == 'admin@tolet.com'){
          this.router.navigate(['/admin']);
        }
        else{
          this.router.navigate(['']);
        }
      },
        error => {
          //console.error(error);
          this.serverError = error.error.error_description != null?
            error.error.error_description : "Woops! There is something wrong. Please try again later.";
          this.isLoading = false;
        });
    return false;
  }

  ngOnInit() {
  }
}
