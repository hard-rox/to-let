import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ProfileViewModel } from 'src/app/app.view-models/profileViewModel';
import { AdvertiseViewModel } from 'src/app/app.view-models/advertiseViewModel';
import { AdminService } from 'src/app/app.services/admin.service';
import { AdvertiseService } from 'src/app/app.services/advertise.service';
import { CommentVM } from 'src/app/app.view-models/commentVM';
import { AdminSetting } from 'src/app/app.view-models/adminSetting';

@Component({
  selector: 'app-admin-pannel',
  templateUrl: './admin-pannel.component.html',
  styleUrls: ['./admin-pannel.component.css']
})
export class AdminPannelComponent implements OnInit {

  allUsers: ProfileViewModel[] = [];
  allAds: AdvertiseViewModel[] = [];
  allComments: CommentVM[] = [];
  pendingComments: CommentVM[] = [];
  allSettings: AdminSetting[] = [];

  constructor(private title: Title,
    private adminService: AdminService,
    private adService: AdvertiseService) { }

  deleteAd(ad: AdvertiseViewModel) {
    if (confirm("Are you sure to delete?")) {
      this.adService.deleteAdvertise(ad).subscribe(
        data => {
          console.log(data);
          location.reload();
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  approveComment(id: number) {
    console.log(id);
    this.adminService.approveComment(id).subscribe(
      data => {
        console.log(data);
        location.reload();
      },
      error => {
        console.log(error);
      }
    );
  }

  approveAd(id: number) {
    console.log(id);
    this.adminService.approveAd(id).subscribe(
      data => {
        console.log(data);
        location.reload();
      },
      error => {
        console.log(error);
      }
    );
  }

  changeSetting(id: number) {
    let setting = this.allSettings.find(s => s.Id == id);
    setting.SettingValue = !setting.SettingValue
    this.adminService.setSettings(setting).subscribe(
      data => {
        console.log(data);
      }
    );
  }

  ngOnInit() {
    this.title.setTitle('ToLet - Admin');

    this.adminService.getAllUsers().subscribe(
      data => {
        console.log(data);
        this.allUsers = data;
      },
      error => {
        console.log(error);
      }
    );

    this.adService.getAllAdvertises(0).subscribe(
      data => {
        console.log(data);
        data.sort((a: AdvertiseViewModel, b: AdvertiseViewModel) => {
          return new Date(b.PostingDate).getTime() - new Date(a.PostingDate).getTime();
        });
        this.allAds = data;
        this.allAds.forEach(ad => {
          ad.Comments.forEach(comment => {
            if (!comment.IsApproved) this.pendingComments.push(comment);
            this.allComments.push(comment);
          });
        });
      },
      error => {
        console.log(error);
      }
    );

    this.adminService.getAllSettings().subscribe(
      data => {
        console.log(data);
        this.allSettings = data;
      }
    );
  }

}
