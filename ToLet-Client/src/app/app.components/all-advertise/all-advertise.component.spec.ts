import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllAdvertiseComponent } from './all-advertise.component';

describe('AllAdvertiseComponent', () => {
  let component: AllAdvertiseComponent;
  let fixture: ComponentFixture<AllAdvertiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllAdvertiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllAdvertiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
