﻿export class RegistrationViewModel {
    public Email: string;
    public Password: string;
    public ConfirmPassword: string;

    constructor(Email: string, Password: string, ConfirmPassword: string) {
        this.Email = Email;
        this.Password = Password;
        this.ConfirmPassword = ConfirmPassword;
    }
}