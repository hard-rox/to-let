﻿namespace ToLet.Models
{
	public class AdminSetting
	{
		public int Id { get; set; }
		public string SettingName { get; set; }
		public bool SettingValue { get; set; }
	}
}
