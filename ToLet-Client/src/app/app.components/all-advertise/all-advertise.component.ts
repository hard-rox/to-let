import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AdvertiseService } from 'src/app/app.services/advertise.service';
import { AdvertiseViewModel } from 'src/app/app.view-models/advertiseViewModel';
import { ActivatedRoute } from '@angular/router';
import { AdvertiseCategoryViewModel } from 'src/app/app.view-models/advertiseCategoryViewModel';
import { Areas } from 'src/app/app.view-models/areas';

@Component({
  templateUrl: './all-advertise.component.html',
})
export class AllAdvertiseComponent implements OnInit {

  searchText: string;
  category: AdvertiseCategoryViewModel = new AdvertiseCategoryViewModel();
  allAds: AdvertiseViewModel[];
  allCategory: Array<AdvertiseCategoryViewModel>;
  totalAds: Array<AdvertiseViewModel>;
  areas: Areas = new Areas();

  rentFrom: number;
  rentTo: number;
  categoryId: number;
  noOfBed: number;
  noOfBath: number;
  area: string;

  searchTermChanged() {
    console.log(this.rentFrom);
    console.log(this.rentTo);
    console.log(this.categoryId);
    console.log(this.noOfBed);
    console.log(this.area);
    this.allAds = new Array<AdvertiseViewModel>();

    this.allAds = this.categoryId > 0 ? this.totalAds.filter(ad => ad.AdvertiseCategoryId == this.categoryId) : this.totalAds;
    this.allAds = this.rentFrom > 0 ? this.allAds.filter(ad => ad.Rent >= this.rentFrom) : this.allAds;
    this.allAds = this.rentTo > 0 ? this.allAds.filter(ad => ad.Rent <= this.rentTo) : this.allAds;
    this.allAds = this.noOfBed > 0 ? this.allAds.filter(ad => ad.NoOfBed == this.noOfBed) : this.allAds;
    //this.allAds = this.noOfBath > 0 ? this.allAds.filter(ad => ad.NoOfBath == this.noOfBath) : this.allAds;
    this.allAds = this.area != "allarea" ? this.allAds.filter(
      ad => ad.Address.toLowerCase().includes(this.area.toLowerCase())
      || ad.Title.toLowerCase().includes(this.area.toLowerCase())
      || ad.Description.toLowerCase().includes(this.area.toLowerCase())
    ) : this.allAds;
    console.log(this.allAds);
    // this.totalAds.forEach(ad => {
    //   // if(ad.Rent >= this.rentFrom || ad.Rent <= this.rentTo
    //   //   || ad.NoOfBed == this.noOfBed || ad.NoOfBath == this.noOfBath){
    //   //   this.allAds.push(ad);
    //   //   console.log(ad);
    //   // }
    //   if()
    // });
  }

  constructor(
    private titleService: Title,
    private activatedRoute: ActivatedRoute,
    private adService: AdvertiseService
  ) { }

  ngOnInit() {
    this.category.Id = parseInt(this.activatedRoute.snapshot.queryParamMap.get("category"));
    this.searchText = this.activatedRoute.snapshot.queryParamMap.get("search");

    this.adService.getCategories().subscribe(
      data => {
        console.log(data);
        this.allCategory = data;
      }
    );

    if (this.category.Id > 0) {
      this.adService.getCategories().subscribe(
        data => {
          this.category = data.find(c => c.Id == this.category.Id);
          this.titleService.setTitle(this.category.Category + ' - ToLet');
        }
      );
      this.adService.getAllAdvertises(this.category.Id).subscribe(
        data => {
          data = data.filter(ad => ad.IsApproved);
          console.log(this.category.Id, data);
          this.allAds = data;
        },
        error => {
          console.log(error);
        }
      );
    }

    if (this.searchText != null) {
      console.log(this.searchText);
      this.adService.getSearchedAdvertises(this.searchText).subscribe(
        data => {
          console.log(data);
          this.titleService.setTitle(this.searchText + ' - ToLet');
          this.allAds = data;
        },
        error => {
          console.log(error);
        }
      );
    }

    if (this.totalAds == null) {
      this.getAd(0);
    }

    //console.log(window.location.href);
    if (window.location.href === 'http://localhost:4200/all-ad') {
      console.log('Alladd no search');
      this.adService.getAllAdvertises(0).subscribe(
        data => {
          console.log(data);
          data.sort((a: AdvertiseViewModel, b: AdvertiseViewModel) => {
            return new Date(b.PostingDate).getTime() - new Date(a.PostingDate).getTime();
          });
          this.allAds = data;
        },
        error => {
          console.log(error);
        }
      );
    }
    console.log(this.allAds, this.totalAds);
  }

  getAd(id: number) {
    this.adService.getAllAdvertises(id).subscribe(
      data => {
        data.sort((a: AdvertiseViewModel, b: AdvertiseViewModel) => {
          return new Date(b.PostingDate).getTime() - new Date(a.PostingDate).getTime();
        });
        console.log(id, data);
        this.totalAds = data;
      },
      error => {
        console.log(error);
      }
    );
  }
}
