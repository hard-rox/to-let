﻿using System.Data.Entity;
using ToLet.Models;

namespace ToLet.DataAccess
{
	public class AdminDataAccess : GenericDataAccess<AdminSetting>
	{
		public AdminDataAccess() : base(ApplicationDbContext.Create())
		{
		}
	}
}
