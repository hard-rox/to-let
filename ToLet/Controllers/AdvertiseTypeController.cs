﻿using System.Collections;
using System.Collections.Generic;
using System.Web.Http;
using ToLet.BusinessLogics;
using ToLet.Models;

namespace ToLet.Controllers
{
    public class AdvertiseTypeController : ApiController
    {
        private AdvertiseTypeLogics advertiseTypeLogics = new AdvertiseTypeLogics();

        // GET: api/AdvertiseType
        public IHttpActionResult Get()
        {
            return Ok(advertiseTypeLogics.GetAllAdvertiseType());
        }
    }
}
