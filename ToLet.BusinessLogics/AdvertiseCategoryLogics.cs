﻿using System.Collections.Generic;
using System.Linq;
using ToLet.DataAccess;
using ToLet.Models;

namespace ToLet.BusinessLogics
{
    public class AdvertiseCategoryLogics
    {
        private AdvertiseCategoryAccess advertiseCategoryAccess = new AdvertiseCategoryAccess();

        public List<AdvertiseCategory> GetAllAdvertiseCategory()
        {
            return advertiseCategoryAccess.GetAll().ToList();
        }
    }
}
