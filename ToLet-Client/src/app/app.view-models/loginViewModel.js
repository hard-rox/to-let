"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LoginViewModel = /** @class */ (function () {
    function LoginViewModel(username, password) {
        this.username = username;
        this.password = password;
        this.grant_type = 'password';
    }
    LoginViewModel.prototype.getBodyString = function () {
        var out = 'username=' + this.username
            + '&password=' + this.password
            + '&grant_type=' + this.grant_type;
        return out;
    };
    return LoginViewModel;
}());
exports.LoginViewModel = LoginViewModel;
//# sourceMappingURL=loginViewModel.js.map