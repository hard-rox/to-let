import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { RegistrationViewModel } from "../app.view-models/registrationViewModel";
import { LoginViewModel } from "../app.view-models/loginViewModel";
import { map } from 'rxjs/operators';
import { ApiBase } from './service-base';
import { AuthToken } from '../app.view-models/auth-token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
    private router: Router, private apiBase: ApiBase) { }

  signUp(model: RegistrationViewModel) {
    var bodyString = JSON.stringify(model);
    var header = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = {headers: header};
    return this.http.post(this.apiBase.baseUri + 'Account/Register', bodyString, options);
  }

  login(model: LoginViewModel) {
    var bodyString = model.getBodyString();

    return this.http.post<AuthToken>(this.apiBase.authUri, bodyString)
      .pipe(map(response => {
        let temp = <AuthToken>response;
        let expiryDate = new Date();
        expiryDate.setDate(expiryDate.getDate() + temp.expires_in / 86400);
        localStorage.setItem('token', temp.access_token);
        localStorage.setItem('userName', temp.userName);
        localStorage.setItem('expires_in', expiryDate.toString());
        this.loggedInEmitter.emit(true);
        // console.log(temp);
        // console.log(expiryDate + '\n' + localStorage.getItem('expires_in'));
        if(temp.userName == "admin@tolet.com"){
          this.router.navigate(['/admin']);
        }
        return true;
      },
        error => {
          console.error(error);
        }
      ))
  }

  @Output()
  loggedInEmitter: EventEmitter<boolean> = new EventEmitter();

  checkLogin(): boolean {
    if(localStorage.getItem('token')  && new Date(localStorage.getItem('expires_in')) < new Date()) this.logout();
    //console.log(localStorage.getItem('token') ? true : false);
    return localStorage.getItem('token') ? true : false;
  }

  getToken() {
    if (this.checkLogin())
      return localStorage.getItem('token');
  }

  getUsername() {
    if (this.checkLogin())
      return localStorage.getItem('userName');
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('userName');
    localStorage.removeItem('expires_in');
    this.loggedInEmitter.emit(false);
    this.router.navigate(['']);
    //console.log('logged out');
    return false;
  }
}
