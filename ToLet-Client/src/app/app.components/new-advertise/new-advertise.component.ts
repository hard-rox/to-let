import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AdvertiseCategoryViewModel } from 'src/app/app.view-models/advertiseCategoryViewModel';
import { AdvertiseTypeViewModel } from 'src/app/app.view-models/advertiseTypeViewModel';
import { AdvertiseService } from 'src/app/app.services/advertise.service';
import { AdvertiseViewModel } from 'src/app/app.view-models/advertiseViewModel';
import { ActivatedRoute, Router } from '@angular/router';
import swal from 'sweetalert2';
import { Picture } from 'src/app/app.view-models/Picture';
import { FileUploader } from 'ng2-file-upload';
import { AdminSetting } from 'src/app/app.view-models/adminSetting';
import { AdminService } from 'src/app/app.services/admin.service';

@Component({
  templateUrl: './new-advertise.component.html',
  styleUrls: ['./new-advertise.component.css']
})
export class NewAdvertiseComponent implements OnInit {

  currentCategoryId: number;
  categories: Array<AdvertiseCategoryViewModel>;
  types: Array<AdvertiseTypeViewModel>;

  pictureFiles: Picture[] = [];
  private adminS: AdminSetting[] = [];

  constructor(private title: Title,
    router: Router,
    private activatedRoute: ActivatedRoute,
    private adService: AdvertiseService,
    private adminService: AdminService) {
    router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  getBase64(event) {
    // let file = event.target.files[0];
    // console.log(file);
    let fileArray = event.target.files;
    for (let index = 0; index < fileArray.length; index++) {
      const file = fileArray[index];
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        //console.log(reader.result);
        let pic: Picture = new Picture();
        pic.ImageData = reader.result;
        pic.Filename = file.name;
        // {
        //   //Id: ,
        //   ImageData: reader.result,
        //   Filename: file.name
        // }
        this.pictureFiles.push(pic);
      }
    }
    //console.log(this.pictureFiles);
  }

  save(saveForm: AdvertiseViewModel) {
    saveForm.AdvertiseCategoryId = this.categories.find(c => c.Id === this.currentCategoryId).Id;
    saveForm.Pictures = Array<Picture>();
    saveForm.Pictures = this.pictureFiles;
    saveForm.PostingDate = new Date();
    saveForm.IsApproved = this.adminS[0].SettingValue;
    console.log(saveForm);
    // swal({
    //   title: 'Saving...',
    //   html: 'Saving your ad. Please wait...',
    //   showConfirmButton: false,
    //   showLoaderOnConfirm: true,
    //   onOpen: () => swal.getConfirmButton().click(),
    //   onClose: () => swal.getCloseButton(),
    //   preConfirm: async () => {
    //     this.adService.saveAdvertise(saveForm)
    //       .subscribe(data => {
    //         console.log(data);
    //       },
    //         error => {
    //           console.log(error);
    //         });
    //   },
    //   allowOutsideClick: () => !swal.isLoading()
    // });

    this.adService.saveAdvertise(saveForm)
      .subscribe(data => {
        console.log(data);
        swal(
          'Saved',
          'Your Advertise has been saved.',
          'success'
        )
      },
        error => {
          console.log(error);
        });
  }

  ngOnInit() {
    this.title.setTitle("ToLet - New Advertise");

    this.currentCategoryId = +this.activatedRoute.snapshot.paramMap.get('category');
    console.log(this.currentCategoryId);

    this.adService.getCategories().subscribe(data => {
      this.categories = data;
      //console.log(data);
    },
      error => {
        console.error(error)
      });

    // this.adService.getTypes().subscribe(data => {
    //   this.types = data;
    //   this.types.forEach(type => {
    //     if (type.Type.toUpperCase() == currentType.toUpperCase()) this.currentType = type;
    //   });
    // },
    //   error => {
    //     console.error(error);
    //   });

    this.adminService.getAllSettings().subscribe(
      data => {
        this.adminS = data;
        console.log(data);
      }
    );
  }

}
