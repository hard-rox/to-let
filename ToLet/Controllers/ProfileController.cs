﻿using Microsoft.AspNet.Identity;
using System.Web.Http;
using ToLet.BusinessLogics;
using ToLet.Models;

namespace ToLet.Controllers
{
	[Authorize]
    public class ProfileController : ApiController
    {
		private UserProfileLogics userProfileLogics = new UserProfileLogics();
		
		[HttpGet]
		public IHttpActionResult GetProfile()
		{
			var id = this.User.Identity.GetUserId();
			var profile = new ProfileViewModel(userProfileLogics.GetUserProfile(id));
			return Ok(profile);
		}

		[HttpGet]
		public IHttpActionResult EditProfile(string name, string shortBio, string address, string phone)
		{
			var id = this.User.Identity.GetUserId();
			var user = userProfileLogics.GetUserProfile(id);
			user.Name = name;
			user.ShortBio = shortBio;
			user.Address = address;
			user.PhoneNumber = phone;

			if (userProfileLogics.UpdateProfile(user)) return Ok();
			return InternalServerError();
		}

		public bool IsInRole(string role)
		{
			return this.User.IsInRole(role);
		}
    }
}
