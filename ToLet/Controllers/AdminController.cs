﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ToLet.BusinessLogics;
using ToLet.Models;

namespace ToLet.Controllers
{
    public class AdminController : ApiController
    {
		private readonly UserProfileLogics userProfileLogics = new UserProfileLogics();
		private readonly AdvertiseLogics advertiseLogics = new AdvertiseLogics();
		private readonly CommentLogics commentLogics = new CommentLogics();
		private readonly AdminLogics adminLogics = new AdminLogics();

		public IHttpActionResult GetUsers()
		{
			var result = userProfileLogics.GetAllUsers();
			return Ok(result);
		}

		[HttpGet]
		public IHttpActionResult ApproveComment(int id)
		{
			if (commentLogics.ApproveComment(id)) return Ok();
			return BadRequest();
		}

		[HttpGet]
		public IHttpActionResult ApproveAd(int id)
		{
			if (advertiseLogics.ApproveAd(id)) return Ok();
			return BadRequest();
		}

		[HttpGet]
		public IHttpActionResult GetSettings()
		{
			var res = adminLogics.GetSettings();
			return Ok(res);
		}

		[HttpPost]
		public IHttpActionResult SetSettings(AdminSetting setting)
		{
			var res = adminLogics.SetSettings(setting);
			return Ok(res);
		}
	}
}
