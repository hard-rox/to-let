﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using ToLet.Models;

namespace ToLet.DataAccess
{
    public class AdvertiseAccess : GenericDataAccess<Advertise>
    {
        public AdvertiseAccess() : base(ApplicationDbContext.Create()) { }

        public override ICollection<Advertise> GetAll()
        {
            var context = ApplicationDbContext.Create();
            context.Configuration.ProxyCreationEnabled = false;
            var res = context.Advertises.AsNoTracking()
                .Include("Pictures").AsNoTracking()
                .Include("Comments")
                .ToList();
            return res;
        }

        public override Advertise GetSingle(Expression<Func<Advertise, bool>> conditionExpression)
        {
            var context = ApplicationDbContext.Create();
            context.Configuration.ProxyCreationEnabled = false;
            var res = context.Advertises.AsNoTracking()
                .Include("Pictures").AsNoTracking()
                .Include("Comments")
                .FirstOrDefault(conditionExpression);
            return res;
        }
    }
}
