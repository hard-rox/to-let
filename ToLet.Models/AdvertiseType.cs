﻿using System.Collections.Generic;

namespace ToLet.Models
{
    public class AdvertiseType
    {
        public int Id { get; set; }
        public string Type { get; set; }

		public ICollection<AdvertiseCategory> Categories { get; set; }
        public ICollection<Advertise> Advertises { get; set; }
	}
}