Use ToLetDb;
GO

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'IIS APPPOOL\DefaultAppPool')
BEGIN
    CREATE USER [IIS APPPOOL\DefaultAppPool] FOR LOGIN [IIS APPPOOL\DefaultAppPool]
    EXEC sp_addrolemember N'db_owner', N'IIS APPPOOL\DefaultAppPool'
END;
GO

grant select on ToLetDb.dbo.__MigrationHistory to public