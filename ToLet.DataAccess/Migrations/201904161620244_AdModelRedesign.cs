namespace ToLet.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdModelRedesign : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Advertises", "IsApproved", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Advertises", "IsApproved");
        }
    }
}
