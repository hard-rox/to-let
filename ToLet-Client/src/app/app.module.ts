import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms"
import { SweetAlert2Module } from "@toverux/ngx-sweetalert2";
import {FileSelectDirective} from "ng2-file-upload";

import { AppComponent } from './app.component';
import { NavbarComponent } from './app.components/navbar/navbar.component';
import { FooterComponent } from './app.components/footer/footer.component';
import { HomeComponent } from './app.components/home/home.component';
import { LoginComponent } from './app.components/login/login.component';
import { SignUpComponent } from './app.components/sign-up/sign-up.component';
import { AdminPannelComponent } from './app.components/admin-pannel/admin-pannel.component';
import { NewAdvertiseComponent } from './app.components/new-advertise/new-advertise.component';
import { MyAccountComponent } from './app.components/my-account/my-account.component';
import { AdvertiseDetailsComponent } from './app.components/advertise-details/advertise-details.component';
import { routes } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ApiBase } from './app.services/service-base';
import { PageNotFoundComponent } from './app.components/page-not-found/page-not-found.component';
import { AllAdvertiseComponent } from './app.components/all-advertise/all-advertise.component';
import { AdvertiseComponent } from './app.components/advertise/advertise.component';

@NgModule({
  declarations: [
    FileSelectDirective,
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    SignUpComponent,
    AdminPannelComponent,
    NewAdvertiseComponent,
    MyAccountComponent,
    AdvertiseDetailsComponent,
    AllAdvertiseComponent,
    AdvertiseComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpClientModule,
    SweetAlert2Module.forRoot(),
    ApiBase
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
