import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/app.services/auth.service';
import { Router } from '@angular/router';
import { AdvertiseCategoryViewModel } from 'src/app/app.view-models/advertiseCategoryViewModel';
import { AdvertiseService } from 'src/app/app.services/advertise.service';
import { Areas } from 'src/app/app.view-models/areas';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  areas: Areas = new Areas();
  
  navHamburgerIsActive: boolean = false;
  searchText: string;

  adCats: AdvertiseCategoryViewModel[];

  constructor(private router: Router, private authService: AuthService, private adService: AdvertiseService) { }

  onSearch(event: KeyboardEvent){
    // console.log(event);
    if(event.key == 'Enter'){
      console.log(this.searchText);
      this.router.navigate(['/all-ad'], {queryParams: {search: this.searchText}});
    }
  }

  isLoggedIn(): boolean {
    return this.authService.checkLogin();
  }

  logout() {
    this.authService.logout();
    return false;
  }

  ngOnInit() {
    this.adService.getCategories().subscribe(
      data =>{
        console.log(data);
        this.adCats = data;
      },
      error => console.log(error)
    );
  }

}
