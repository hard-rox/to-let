﻿namespace ToLet.Models
{
    public class Picture
    {
        public int Id { get; set; }
        public string ImageData { get; set; }
        public string Filename { get; set; }

        public int AdvertiseId { get; set; }
        public virtual Advertise Advertise { get; set; }
    }
}
