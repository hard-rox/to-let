import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ProfileViewModel } from 'src/app/app.view-models/profileViewModel';
import { ProfileService } from 'src/app/app.services/profile.service';
import swal from 'sweetalert2';
import { AdvertiseViewModel } from 'src/app/app.view-models/advertiseViewModel';
import { AdvertiseService } from 'src/app/app.services/advertise.service';

@Component({
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {

  private isModalActive: boolean = false;
  private unApproved: number = 0;
  private user: ProfileViewModel;

  private image: any;

  constructor(private title: Title,
    private profileService: ProfileService,
    private adService: AdvertiseService) {
    this.user = new ProfileViewModel();
  }

  deleteAd(ad: AdvertiseViewModel) {
    if (confirm("Are you sure to delete?")) {
      this.adService.deleteAdvertise(ad).subscribe(
        data => {
          console.log(data);
          location.reload();
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  onFileChange(event) {
    let img = event.target.files[0];
    console.log(img);
    //this.image = img.name;
  }

  editProfile(formData: ProfileViewModel) {
    console.log(formData);
    if(formData.Name != undefined) this.user.Name = formData.Name;
    if(formData.ShortBio != undefined) this.user.ShortBio = formData.ShortBio;
    if(formData.Address != undefined) this.user.Address = formData.Address;
    if(formData.PhoneNo != undefined) this.user.PhoneNo = formData.PhoneNo;

    swal({
      title: 'Saving...',
      html: 'Saving is performing...',
      showConfirmButton: false,
      showLoaderOnConfirm: true,
      onOpen: () => swal.getConfirmButton().click(),
      onClose: () => {
        swal('Saved', 'Your profile data has been changed.', 'success');
      },
      preConfirm: async () => {
        await this.profileService.updateProfile(this.user).toPromise().then(
          data => {
            console.log(data);
          },
          error => console.error(error)
        );
      },
      allowOutsideClick: () => !swal.isLoading()
    });
    this.isModalActive = false;
  }

  ngOnInit() {
    this.title.setTitle('ToLet - My Profile');

    swal({
      title: 'Loading...',
      html: 'Profile data is on the way...',
      showConfirmButton: false,
      showLoaderOnConfirm: true,
      onOpen: () => swal.getConfirmButton().click(),
      onClose: () => { },
      preConfirm: async () => {
        await this.profileService.geProfile().toPromise().then(data => {
          this.user = data;
          this.unApproved = data.Advertises.filter(ad => !ad.IsApproved).length;
          console.log(data);
          // console.log(typeof data);
          // console.log(typeof this.user);
          // console.log(this.user);
        }, error => {
          console.error(error);
          // swal({
          //   type: 'error',
          //   title: 'Oops...',
          //   text: 'Something went wrong!',
          //   showConfirmButton: true,
          //   confirmButtonText: 'Ok'
          // });
        });
      },
      allowOutsideClick: () => !swal.isLoading()
    }
    );
  }
}
