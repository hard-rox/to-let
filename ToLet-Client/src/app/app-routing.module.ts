import { Routes } from "@angular/router";
import { AllAdvertiseComponent } from "./app.components/all-advertise/all-advertise.component";
import { HomeComponent } from "./app.components/home/home.component";
import { LoginComponent } from "./app.components/login/login.component";
import { SignUpComponent } from "./app.components/sign-up/sign-up.component";
import { AdminPannelComponent } from "./app.components/admin-pannel/admin-pannel.component";
import { NewAdvertiseComponent } from "./app.components/new-advertise/new-advertise.component";
import { AuthRouteGuard, AdminRouteGuard } from "./app.routeGuards/auth-route.guard";
import { MyAccountComponent } from "./app.components/my-account/my-account.component";
import { AdvertiseDetailsComponent } from "./app.components/advertise-details/advertise-details.component";
import { PageNotFoundComponent } from "./app.components/page-not-found/page-not-found.component";

export const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },

    {
        path: 'signup',
        component: SignUpComponent
    },

    {
        path: 'login',
        component: LoginComponent
    },

    {
        path: 'all-ad',
        component: AllAdvertiseComponent
    }, 

    {
        path: 'advertise-details/:id',
        component: AdvertiseDetailsComponent
    },

    {
        path: 'create/:category',
        component: NewAdvertiseComponent,
        canActivate: [AuthRouteGuard]
    },

    {
        path: 'my-account',
        component: MyAccountComponent
    },

    {
        path: 'admin',
        component: AdminPannelComponent,
        canActivate: [AdminRouteGuard]
    },

    {
        path: '**',
        component: PageNotFoundComponent
    }
]
