﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using ToLet.Models;

namespace ToLet.DataAccess
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<AdvertiseCategory> AdvertiseCategories { get; set; }
        public DbSet<Advertise> Advertises { get; set; }
		public DbSet<AdminSetting> AdminSettings { get; set; }
	}
}