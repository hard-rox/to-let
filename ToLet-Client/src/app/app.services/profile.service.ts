import { Injectable } from '@angular/core';
import { ApiBase } from './service-base';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProfileViewModel } from '../app.view-models/profileViewModel';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  profileApi: string = this.apiBase.baseUri + 'Profile/';

  constructor(private apiBase: ApiBase,
              private httpClient: HttpClient,
              private authService: AuthService) { }

  geProfile(){
    let options = {headers: new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization' : 'bearer ' + this.authService.getToken()
    })}
    return this.httpClient.get<ProfileViewModel>(this.profileApi + 'GetProfile', options);
  }

  updateProfile(profile: ProfileViewModel){
    //let body = JSON.stringify(profile);
    console.log(profile);
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + this.authService.getToken()
      })
    }
    return this.httpClient.get(this.profileApi + 'EditProfile?name='+profile.Name+'&shortBio='+profile.ShortBio+'&address='+profile.Address+'&phone='+profile.PhoneNo, options);
  }
}
