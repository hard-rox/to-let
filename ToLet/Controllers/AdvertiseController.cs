﻿using Microsoft.AspNet.Identity;
using System;
using System.Web.Http;
using ToLet.BusinessLogics;
using ToLet.Models;

namespace ToLet.Controllers
{
    public class AdvertiseController : ApiController
    {
        private readonly AdvertiseLogics _advertiseLogics = new AdvertiseLogics();
		private readonly CommentLogics _commentLogics = new CommentLogics();

        // GET: api/Advertise
        public IHttpActionResult GetAllAdvertises(int id = 0)
		{
			var result = _advertiseLogics.GetAllAdvertises(id);
			return Ok(result);
		}

		// GET: api/Advertise
		[HttpGet]
		public IHttpActionResult SearchAd(string searchText)
		{
			return Ok(_advertiseLogics.SearchAdvertise(searchText));
		}

        // GET: api/Advertise/5
        public IHttpActionResult Get(int id)
		{
			return Ok(_advertiseLogics.GetAdvertiseById(id));
		}

		//[Authorize]
		// POST: api/Advertise
		public IHttpActionResult CreateAdvertise([FromBody]Advertise advertise)
		{
		    if (!ModelState.IsValid)
				return BadRequest(ModelState);

			try
			{
				var id = this.User.Identity.GetUserId();
				advertise.PropertyOwnerId = id;
				if (_advertiseLogics.SaveAdvertise(advertise))
					return Created("Advertise/" + advertise.Id, advertise);
				else return InternalServerError();
			}
			catch (Exception ex)
			{
				return InternalServerError(ex);
			}
        }

        // PUT: api/Advertise/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Advertise/5
        public IHttpActionResult Delete(int id)
        {
            var ad = _advertiseLogics.GetAdvertiseById(id);
            var res = _advertiseLogics.DeleteAdvertise(ad);
            if (res) return Ok();
            return BadRequest();
        }

		public void PostComment([FromBody] Comment comment)
		{
			var userId = this.User.Identity.GetUserId();
			comment.UserId = userId;
			comment.PostingTime = DateTime.Now;

			_commentLogics.PostComment(comment);
		}
    }
}
