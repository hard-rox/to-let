﻿using System.Collections.Generic;
using System.Linq;
using ToLet.DataAccess;
using ToLet.Models;

namespace ToLet.BusinessLogics
{
	public class UserProfileLogics
	{
		private UserDataAccess userAccess = new UserDataAccess();

		public List<ApplicationUser> GetAllUsers()
		{
			return userAccess.GetAll().ToList();
		}

		public ApplicationUser GetUserProfile(string userId)
		{
			return userAccess.GetSingle(u => u.Id == userId);
		}

		public bool UpdateProfile(ApplicationUser user)
		{
			return userAccess.Update(user);
		}
	}
}
