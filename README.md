# T-Let

This project is Guinea pig of my Angular 4 learning mission. This is a All types of apertment and space renting and saling application. Every authorised user can post their advertise in this app and interested buyer can view the information and contact info of the owner.

## Getting Started

To test the application just download full repository, open in Visual Studio 2015 or later, Restore all packages from ```package.json``` & ```packages.config``` and run. 

### Prerequisites

To run this application you need
* .NET 4.6
* SQL Server 2012 or later
* Visual Studio 2015 or later
* Node.js
in your Computer.

## Built With

* ASP.NET MVC 5
* ASP.NET Web API
* Angular 4
* Visual C#
* Razor View Engine
* HTML5
* CSS
* Bootstrap 4
* SQL Server 2012
* .NET Framework 4.6
* Visual Studio 2017

## Some ScreenShots

![Home](Screenshots/Home   ToLet.png?raw=true "Home")

## Authors

* **Rasedur Rahman Roxy** - [In Facebook](https://facebook.com/MdRoxy)