﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToLet.DataAccess;
using ToLet.Models;

namespace ToLet.BusinessLogics
{
	public class AdvertiseLogics
	{
		private readonly AdvertiseAccess _advertiseAccess = new AdvertiseAccess();
		private readonly AdvertiseCategoryAccess _advertiseCategoryAccess = new AdvertiseCategoryAccess();
		private readonly UserDataAccess _userAccess = new UserDataAccess();

		public List<Advertise> GetAllAdvertises(int id)
		{
			if (id == 0) return _advertiseAccess.GetAll().ToList();
			var category = _advertiseCategoryAccess.GetSingle(c => c.Id == id);
			return _advertiseAccess.GetAll().Where(a => a.AdvertiseCategoryId == category.Id).ToList();
		}

		public Advertise GetAdvertiseById(int id)
		{
			return _advertiseAccess.GetSingle(ad => ad.Id == id);
		}

		public List<Advertise> GetAdvertisesByUser(string userId)
		{
			return _userAccess.GetSingle(u => u.Id == userId).Advertises.ToList();
		}

		public bool ApproveAd(int id)
		{
			var ad = _advertiseAccess.GetSingle(c => c.Id == id);
			ad.IsApproved = true;
			return _advertiseAccess.Update(ad);
		}

		public List<Advertise> SearchAdvertise(string searchText)
		{
			var ads = _advertiseAccess.GetAll();
			return ads.Where(item => item.Title.ToLower().Contains(searchText.ToLower())
			|| item.Description.ToLower().Contains(searchText.ToLower())
			|| item.Address.ToLower().Contains(searchText.ToLower())).ToList();
		}

		public bool SaveAdvertise(Advertise advertise)
		{
			return _advertiseAccess.Add(advertise);
		}

		public bool DeleteAdvertise(Advertise advertise)
		{
			return _advertiseAccess.Delete(advertise);
		}
	}
}
