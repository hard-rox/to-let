import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvertiseViewModel } from 'src/app/app.view-models/advertiseViewModel';
import { AdvertiseService } from 'src/app/app.services/advertise.service';
import { Title } from '@angular/platform-browser';
import { CommentVM } from 'src/app/app.view-models/commentVM';
import { AdminService } from 'src/app/app.services/admin.service';
import { AdminSetting } from 'src/app/app.view-models/adminSetting';
import { ProfileViewModel } from 'src/app/app.view-models/profileViewModel';
import { ProfileService } from 'src/app/app.services/profile.service';

@Component({
  templateUrl: './advertise-details.component.html',
  styleUrls: ['./advertise-details.component.css']
})
export class AdvertiseDetailsComponent implements OnInit {

  private user: ProfileViewModel;
  private advertise: AdvertiseViewModel;
  private commentText: string;
  private adminS: AdminSetting[] = [];

  constructor(private titleService: Title,
    private activatedRoute: ActivatedRoute,
    private adService: AdvertiseService,
    private adminService: AdminService,
    private profileS: ProfileService
  ) { }

  ngOnInit() {
    this.getAdvertise();
    //this.titleService.setTitle('To-Let - ' + this.advertise.Title);

    this.adminService.getAllSettings().subscribe(
      data => {
        this.adminS = data;
      }
    );
  }

  private getAdvertise(){
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(id);
    this.adService.getAdvertise(+id)
    .subscribe(
      result => {
        this.advertise = result;
        this.advertise.Comments = this.advertise.Comments.filter(c => c.IsApproved == true);
        this.profileS.geProfile().subscribe(
          data => {
            this.user = data;
          }
        );
        console.log(result);
      },
      error => {
        console.error(error);
      }
    )
  }

  private postComment(){
    let commentVM =  new CommentVM();
    commentVM.AdId = this.advertise.Id;
    commentVM.CommentText = this.commentText;
    commentVM.IsApproved = this.adminS[1].SettingValue;
    console.log(commentVM);

    this.adService.postComment(commentVM).subscribe(
      data => {
        console.log(data);
        this.commentText = "";
      },
      error => {
        console.log(error);
      }
    )
  }

}
