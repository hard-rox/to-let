import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAdvertiseComponent } from './new-advertise.component';

describe('NewAdvertiseComponent', () => {
  let component: NewAdvertiseComponent;
  let fixture: ComponentFixture<NewAdvertiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewAdvertiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAdvertiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
