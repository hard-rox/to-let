﻿using System.Collections.Generic;

namespace ToLet.Models
{
    public class AdvertiseCategory
    {
        public int Id { get; set; }
        public string Category { get; set; }
		public string IconName { get; set; }

		public int TypeId { get; set; }

        public ICollection<Advertise> Advertises { get; set; }
	}
}