"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RegistrationViewModel = /** @class */ (function () {
    function RegistrationViewModel(Email, Password, ConfirmPassword) {
        this.Email = Email;
        this.Password = Password;
        this.ConfirmPassword = ConfirmPassword;
    }
    return RegistrationViewModel;
}());
exports.RegistrationViewModel = RegistrationViewModel;
//# sourceMappingURL=registrationViewModel.js.map