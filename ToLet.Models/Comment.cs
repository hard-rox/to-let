﻿using System;

namespace ToLet.Models
{
	public class Comment
	{
		public int Id { get; set; }
		public string CommentText { get; set; }
		public DateTime PostingTime { get; set; }
		public bool IsApproved { get; set; }
		public string UserId { get; set; }
		public int AdId { get; set; }

		public virtual Advertise Ad { get; set; }
		public virtual ApplicationUser User { get; set; }
	}
}
