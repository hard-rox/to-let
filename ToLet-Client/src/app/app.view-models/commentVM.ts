export class CommentVM {
        Id: number;
        CommentText: string;
        PostingTime: Date;
        IsApproved: boolean;
        UserId: string;
        AdId: number;
    }