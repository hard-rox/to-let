﻿using System.Collections.Generic;
using System.Linq;
using ToLet.DataAccess;
using ToLet.Models;

namespace ToLet.BusinessLogics
{
	public class AdminLogics
	{
		private AdminDataAccess adminDataAccess = new AdminDataAccess();
		public List<AdminSetting> GetSettings()
		{
			var res = adminDataAccess.GetAll().ToList();
			return res;
		}

		public bool SetSettings(AdminSetting setting)
		{
			var res = adminDataAccess.Update(setting);
			return res;
		}
	}
}
