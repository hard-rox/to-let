import { OnInit, Component, Input } from "@angular/core";
import { AdvertiseViewModel } from "src/app/app.view-models/advertiseViewModel";

@Component({
    selector: 'advertise',
    templateUrl: './advertise.component.html',
    //styleUrls: ['./advertise.component.css']
})
export class AdvertiseComponent implements OnInit {

    @Input() advertise: AdvertiseViewModel;

    constructor() {}

    ngOnInit(): void {
        //console.log(this.advertise)
    }
}