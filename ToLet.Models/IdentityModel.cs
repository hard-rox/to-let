﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Security.Claims;

namespace ToLet.Models
{
    public class ApplicationUser : IdentityUser
    {
		public string Name { get; set; }
		public string ShortBio { get; set; }
		public string Address { get; set; }
		public string ProfilePhoto { get; set; }

		public virtual ICollection<Advertise> Advertises { get; set; }
		public virtual ICollection<Comment> Comments { get; set; }
		public async System.Threading.Tasks.Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}