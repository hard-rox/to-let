﻿export class LoginViewModel {
    username: string;
    password: string;
    grant_type: string;

    constructor(username: string, password: string) {
        this.username = username;
        this.password = password;
        this.grant_type = 'password';
    }

    getBodyString(): string {
        var out = 'username=' + this.username
            + '&password=' + this.password
            + '&grant_type=' + this.grant_type;
        return out;
    }
}