import { AdvertiseViewModel } from "./advertiseViewModel";

export class ProfileViewModel {
    public Id: string;
    public Name: string;
    public ShortBio: string;
    public Address: string;
    public Email: string;
    public PhoneNo: string;
    public ProfileImage: string;

    public Advertises: AdvertiseViewModel[];
}