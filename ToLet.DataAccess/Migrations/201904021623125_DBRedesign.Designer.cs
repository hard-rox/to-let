// <auto-generated />
namespace ToLet.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class DBRedesign : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DBRedesign));
        
        string IMigrationMetadata.Id
        {
            get { return "201904021623125_DBRedesign"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
