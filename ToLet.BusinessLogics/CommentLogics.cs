﻿using ToLet.DataAccess;
using ToLet.Models;

namespace ToLet.BusinessLogics
{
	public class CommentLogics
	{
		private CommentAccess commentAccess = new CommentAccess();

		public bool PostComment(Comment comment)
		{
			return commentAccess.Add(comment);
		}

		public bool ApproveComment(int id)
		{
			var comment = commentAccess.GetSingle(c => c.Id == id);
			comment.IsApproved = true;
			return commentAccess.Update(comment);
		}
	}
}
