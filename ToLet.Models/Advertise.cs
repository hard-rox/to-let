﻿using System;
using System.Collections.Generic;

namespace ToLet.Models
{
    public class Advertise
    {
        public int Id { get; set; }
		public string Title { get; set; }
		public string Address { get; set; }
		public double Rent { get; set; }
		public double Size { get; set; }
		public int NoOfBed { get; set; }
		public int NoOfBath { get; set; }
		public string Description { get; set; }
		public bool IsApproved { get; set; }

		public bool HasDrawingRoom { get; set; }
		public bool HasDiningRoom { get; set; }
		public bool HasCarParking { get; set; }
		public bool HasLift { get; set; }
		public bool HasGas { get; set; }
		public bool HasGenerator { get; set; }
		public bool IsElectrickBillIncluded { get; set; }
		public DateTime PostingDate { get; set; }

        public int AdvertiseCategoryId { get; set; }
        public virtual AdvertiseCategory AdvertiseCategory { get; set; }

		public string PropertyOwnerId { get; set; }
		public virtual ApplicationUser PropertyOwner { get; set; }

		public ICollection<Comment> Comments { get; set; }
        public ICollection<Picture> Pictures { get; set; }
	}
}