import { Component } from '@angular/core';
import { AuthService } from './app.services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ToLet-Web';

  isLoggedIn: boolean = this.accService.checkLogin();

    constructor(private accService: AuthService) {
        this.accService.loggedInEmitter.subscribe({
            next: (data: boolean) => this.isLoggedIn = data 
        });
    }

    logout() {
        this.accService.logout();
        return false;
    }
}
