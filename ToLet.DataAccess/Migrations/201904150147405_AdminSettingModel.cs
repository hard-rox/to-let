namespace ToLet.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdminSettingModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdminSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SettingName = c.String(),
                        SettingValue = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AdminSettings");
        }
    }
}
