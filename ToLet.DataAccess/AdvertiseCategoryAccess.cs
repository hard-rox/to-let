﻿using ToLet.Models;

namespace ToLet.DataAccess
{
    public class AdvertiseCategoryAccess: GenericDataAccess<AdvertiseCategory>
    {
        public AdvertiseCategoryAccess() : base(ApplicationDbContext.Create()) { }
    }
}
