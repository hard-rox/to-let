﻿using System.Collections.Generic;
using System.Linq;
using ToLet.DataAccess;
using ToLet.Models;

namespace ToLet.BusinessLogics
{
    public class AdvertiseTypeLogics
    {
        private AdvertiseTypeAccess advertiseTypeAccess = new AdvertiseTypeAccess();

        public List<AdvertiseType> GetAllAdvertiseType()
        {
            return advertiseTypeAccess.GetAll().ToList();
        }
    }
}
