import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { AuthService } from '../app.services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminRouteGuard implements CanActivate {

  constructor(private router: Router,
    private authService: AuthService) {}

  canActivate(): boolean{
    if(this.authService.getUsername() == "admin@tolet.com"){
        return true;
    }
    else{
      this.router.navigate(['/login']);
      return false;
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthRouteGuard implements CanActivate {

  constructor(private router: Router,
    private authService: AuthService) {}

  canActivate(): boolean{
    if(!this.authService.checkLogin()){
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
