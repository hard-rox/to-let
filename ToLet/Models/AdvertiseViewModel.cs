﻿using System;
using System.Collections.Generic;

namespace ToLet.Models
{
	[Serializable]
	public class AdvertiseViewModel
	{
		public int Id { get; set; }
		public string PropertyName { get; set; }
		public string ShortOverview { get; set; }
		public double Size { get; set; }
		public int Beds { get; set; }
		public int Baths { get; set; }
		public int Parkings { get; set; }
		public double Price { get; set; }
		public string VideoOverview { get; set; }
		public string Address { get; set; }

		public string PropertyOwnerId { get; set; }
		public int PropertyTypeId { get; set; }
		public int PropertyCategoryID { get; set; }
		

		//public AdvertiseViewModel(Advertise advertise)
		//{
		//	this.Id = advertise.Id;
		//	this.PropertyName = advertise.PropertyName;
		//	this.Size = advertise.Size;
		//	this.ShortOverview = advertise.ShortOverview;
		//	this.Beds = advertise.Beds;
		//	this.Baths = advertise.Baths;
		//	this.Parkings = advertise.Parkings;
		//	this.Price = advertise.Price;
		//	this.VideoOverview = advertise.VideoOverview;
		//	this.Address = advertise.Address;
		//	this.Images = advertise.Images;
		//	this.PropertyOwnerId = advertise.PropertyOwnerId;
		//	this.PropertyTypeId = advertise.PropertyTypeId;
		//	this.PropertyCategoryID = advertise.PropertyCategoryID;
		//	this.VisitRequests = advertise.VisitRequests;
		//}

		public AdvertiseViewModel() { }
	}
}