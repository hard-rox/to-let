﻿export class SearchCriteria {
    public Type: number;
    public Category: number;
    public Location: string;
    public MinSize: number;
    public MaxSize: number;
    public Bed: number;
    public Bath: number;
    public MinPrice: number;
    public MaxPrice: number;
}