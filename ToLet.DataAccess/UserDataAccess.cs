﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using ToLet.Models;

namespace ToLet.DataAccess
{
	public class UserDataAccess : GenericDataAccess<ApplicationUser>
	{
		public UserDataAccess() : base(ApplicationDbContext.Create()) { }

	    public override ApplicationUser GetSingle(Expression<Func<ApplicationUser, bool>> conditionExpression)
	    {
	        var context = ApplicationDbContext.Create();
	        context.Configuration.ProxyCreationEnabled = false;
	        var res = context.Users.AsNoTracking().FirstOrDefault(conditionExpression);
	        var ads = context.Advertises.AsNoTracking()
                .Include("Pictures")
                .Where(ad => ad.PropertyOwnerId == res.Id);
	        res.Advertises = ads.ToList();
            return res;
	    }
    }
}
