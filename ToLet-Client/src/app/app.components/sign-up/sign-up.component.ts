import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../../app.services/auth.service';
import { Router } from '@angular/router';
import { RegistrationViewModel } from '../../app.view-models/registrationViewModel';
import { LoginViewModel } from '../../app.view-models/loginViewModel';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private titleService: Title,
    private accountService: AuthService,
    private router: Router) { }

  public isLoading = false;
  errorList: string[] = [];

  onSubmit(formData: any) {
    this.errorList = [];
    var model = new RegistrationViewModel(formData.email, formData.password, formData.confirmPassword);
    console.log(JSON.stringify(model));

    if (model.Password != model.ConfirmPassword) {
      this.errorList.push("Passwords doesn't match.");
      return false;
    }

    this.isLoading = true;

    this.accountService.signUp(model)
      .subscribe(
        response => {
          this.isLoading = false;
          //console.log(response);
          this.accountService.login(new LoginViewModel(model.Email, model.Password))
            .subscribe(data => {
              this.router.navigate(['']);
            },
              error => {
                //console.error(error);
                this.errorList.push(error.error.error_description != null ?
                  error.error.error_description : "Woops! There is something wrong. Please try again later.");
                //this.isLoading = false;
              });
        },
        error => {
          this.isLoading = false;
          this.errorList.push(error.error.Message);
          for(var key in error.error.ModelState)
          {
            this.errorList.push(error.error.ModelState[key]);
          }
          console.log(JSON.stringify(error));
        });

    return false;
  }

  ngOnInit() {
  }

}
